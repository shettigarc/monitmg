class Metric < ActiveRecord::Base
  has_many :dash_metrics

  enum widget_type: { widget_image: 0, widget_iframe: 1, widget_json: 2 }
end
