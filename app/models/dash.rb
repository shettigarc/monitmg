class Dash < ActiveRecord::Base
  has_many :dash_metrics
  has_many :metrics, through: :dash_metrics
end
