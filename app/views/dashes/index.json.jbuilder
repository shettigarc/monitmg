json.array!(@dashes) do |dash|
  json.extract! dash, :id, :name
  json.url dash_url(dash, format: :json)
end
