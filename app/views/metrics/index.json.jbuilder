json.array!(@metrics) do |metric|
  json.extract! metric, :id, :title, :uri_path
  json.url metric_url(metric, format: :json)
end
