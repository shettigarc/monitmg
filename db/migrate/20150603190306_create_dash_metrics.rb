class CreateDashMetrics < ActiveRecord::Migration
  def change
    create_table :dash_metrics do |t|
      t.integer :dash_id
      t.integer :metric_id

      t.timestamps null: false
    end
  end
end
