class AddRefreshIntervalToDash < ActiveRecord::Migration
  def change
    add_column :dashes, :refresh_interval, :integer
  end
end
