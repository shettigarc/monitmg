class CreateMetrics < ActiveRecord::Migration
  def change
    create_table :metrics do |t|
      t.string :title
      t.text :widget_data
      t.integer :widget_type

      t.timestamps null: false
    end
  end
end
